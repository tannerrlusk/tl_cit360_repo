package w04;

import org.json.JSONArray;
import org.json.JSONObject;

/*
I followed along this video to create this Practice Class
https://www.youtube.com/watch?v=qzRKa8I36Ww
These videos show how to create a local https server:
https://www.youtube.com/watch?v=fdQYdLANpF0 (Mac OS)
https://www.youtube.com/watch?v=gpSK0CbSu2g (Windows)

 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class JsonHttpPractice1 {

    private static HttpURLConnection connection;

    public static void main(String[] args) {
        // Using java.net.HttpURLConnection
        BufferedReader reader;
        String line;
        StringBuffer responseContent = new StringBuffer();

        try {
            URL url = new URL("http://localhost/bookdata.json");
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            int status = connection.getResponseCode();
            System.out.println(status);

            if (status > 299) {
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                reader.close();
            }
                else {
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                reader.close();
                }

                System.out.println(responseContent.toString());

                //parse(responseContent.toString());
        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

    }

    /*
    public static String parse(String responseBody){
        JSONArray books = new JSONArray(responseBody);
        for (int i = 0; i <books.length(); i++){
            JSONObject book = books.getJSONObject(i);
            String title =  books.;
        }
        return null;
    }
     */
}
