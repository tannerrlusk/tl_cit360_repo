package w03;

import java.util.Scanner;
import java.util.*;


public class ExceptionHandling {
    /*
    Use the following requirements when writing your program:
    Create a program that gathers input of two numbers from the keyboard in its main method and then calls the method below to calculate.
    Write a method that divides the two numbers together (num1/num2) and returns the result. The method should include a throws clause for the most specific exception possible if the user enters zero for num2.
    In your main method, use exception handling to catch the most specific exception possible. Display a descriptive message in the catch to tell the user what the problem is. Give the user a chance to retry the data entry. Display a message in the final statement.
    Run and show the output when there is no exception and when a zero in the denominator is used.
     */
    public static void main(String[] args) {
        //Create a Scanner Object that will be able to receive input for the user
        Scanner keyboard = new Scanner(System.in);
        //Since the numbers can have decimals, variables will be of type float
        float firstNum = 0;
        float secondNum = 0;
        float quotient = 0;
        //Boolean must be switched to true in order to escape the while loop
        boolean firstInputSuccessful = false;
        boolean secondInputSuccessful = false;
        boolean secondInputNotZero = false;

        //Summary
        System.out.println("\nThe user will enter two numbers, and the program will return the result of the first");
        System.out.println("number divided by the latter. Only numbers will be accepted.");

        //In each of these do while loops there are corresponding try and catch statement. If the user
        //provides invalid input, an error will be thrown and caught by the catch statement, and the
        //do while loop cannot be exited until an error isn't thrown.
        do {
            System.out.println("\nPlease enter your first number:");
            try {
                firstNum = keyboard.nextFloat();
                firstInputSuccessful = true;
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. This must be a number.");
                keyboard.nextLine();
            }
            }while (!firstInputSuccessful) ;

            do {
                do {
                    System.out.println("\nNow, please enter a second number that isn't zero:");
                    try {
                        secondNum = keyboard.nextFloat();
                        secondInputSuccessful = true;
                    } catch (InputMismatchException e) {
                        System.out.println("Invalid input. Both inputs must be numbers!");
                        keyboard.next();
                    }
                } while (!secondInputSuccessful);


                try {
                    //Apparently, dividing a float by a 0.0 float does not throw an error, and earlier tries
                    //displayed Infinity. Therefore, I needed to add an if statement that would throw the
                    //Arithmetic Exception if a 0 was entered.
                    if (secondNum == 0){
                        throw new ArithmeticException();
                    }
                    float result = divideFloats(firstNum, secondNum);
                    System.out.println("\n" + firstNum + " divided by " + secondNum + " results in a quotient of: " + result + ".");
                    secondInputNotZero = true;
                }catch(ArithmeticException e){
                    System.out.println("Invalid input. Program will not accept 0 for the second number!");
                }

            }while(!secondInputNotZero);

    }
    //This function will divide the two received numbers and return the quotient
    public static float divideFloats ( float firstNum, float secondNum){
        float quotient;
        quotient = firstNum / secondNum;
        return quotient;

    }
}