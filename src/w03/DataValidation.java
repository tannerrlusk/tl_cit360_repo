package w03;
import java.util.Scanner;

public class DataValidation {
    /*
    Rewrite the data collection so that it uses data validation to ensure an error does not occur. If the user does type a zero in the denominator, display a message and give the user a chance to retry.
    Run and show the output when there is no problem with data entry and when a zero is used in the denominator.
     */

    public static void main(String[] args) {
        //Create a Scanner Object that will be able to receive input for the user
        Scanner keyboard = new Scanner(System.in);
        //Since the numbers can have decimals, variables will be of type float
        float firstNum = 0;
        float secondNum = 0;
        float quotient = 0;
        //Boolean must be switched to true in order to escape the while loop
        boolean firstInputSuccessful = false;
        boolean secondInputSuccessful = false;

        //Summary
        System.out.println("\nThe user will enter two numbers, and the program will return the result of the first");
        System.out.println("number divided by the latter. Only numbers will be accepted.");

        //For Data Validation, instead of an error being thrown, the .hasNextFloat() method for the Scanner object will return
        //true only if a float, basically any number, is entered. The data is validated by only changing the boolean variables
        //to true if the specified data type is entered, else they remain in that loop.
        do {
            System.out.println("\nPlease enter your first number:");
            if(keyboard.hasNextFloat()){
                firstNum = keyboard.nextFloat();

                do {
                    System.out.println("\nNow, please enter a second number that isn't zero:");
                    if (keyboard.hasNextFloat()) {
                        secondNum = keyboard.nextFloat();

                        if (secondNum == 0) {
                            System.out.println("The second number must not be zero.\n");
                            keyboard.nextLine();
                        } else {
                            secondInputSuccessful = true;
                            firstInputSuccessful = true;
                        }
                    } else {
                        System.out.println("Invalid input. Please enter a number!");
                        keyboard.next();
                    }
                }while(!secondInputSuccessful);
            } else {
                System.out.println("Invalid input. Please enter a number!");
                keyboard.nextLine();
            }

        } while(!firstInputSuccessful);

        float result = divideFloats(firstNum,secondNum);
        System.out.println("\n"+ firstNum + " divided by " + secondNum + " results in a quotient of: " + result + ".");

    }

    //This function will divide the two numbers provided by the user. No validation required here,
    //as the previous validation requires that this function will only receive valid input.
    public static float divideFloats(float firstNum, float secondNum){
        float quotient;
        quotient = firstNum / secondNum;
        return quotient;
    }
}
