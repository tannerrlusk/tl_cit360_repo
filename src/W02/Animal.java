package W02;

public class Animal {

    private String species;
    private String diet;
    private int averageWeightLBS;

    public Animal(String species, String diet, int averageWeightLBS){
        this.species = species;
        this.diet = diet;
        this.averageWeightLBS = averageWeightLBS;

    }

    public String toString(){
        return "Species: " + species + "  Diet: " + diet + "  Average Weight: " + averageWeightLBS + " lbs.";
    }

    public int getAverageWeight(){
        return averageWeightLBS;
    }
}
