package W02;

import java.util.*;

class AnimalComparator implements Comparator<Animal> {
    @Override
    public int compare(Animal a1, Animal a2) {
        if (a1.getAverageWeight() > a2.getAverageWeight()){
            return 1;
        }
        else if (a1.getAverageWeight() < a2.getAverageWeight()){
            return -1;
        }
        else
            return 0;
    }
}
