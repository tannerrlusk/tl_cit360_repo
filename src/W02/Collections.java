package W02;

import java.util.*;

public class Collections {

    public static void main(String[] args) {


    //The following demonstrate that ArrayLists can contain different data types
    List firstArrayList = new ArrayList();
    firstArrayList.add("List");
    firstArrayList.add("with");
    firstArrayList.add("Strings");
    firstArrayList.add("only");

    List secondArrayList = new ArrayList();
    secondArrayList.add(7);
    secondArrayList.add("elephant that isn't an integer");
    secondArrayList.add(9);
    secondArrayList.add(10);

    System.out.println("---Examples of ArrayList---");

    for (Object str : firstArrayList){
        System.out.println(str);
    }
    System.out.println("\n");
    for (Object integer : secondArrayList){
        System.out.println(integer);
    }

    //Tree Set
        System.out.println("\n");
        System.out.println("---Example of Tree Set---");
    Set letterSet = new TreeSet();
    letterSet.add("c");
    letterSet.add("a");
    letterSet.add("b");
    letterSet.add("a");
    letterSet.add("C");
    letterSet.add("B");
    letterSet.add("A");

    for (Object str : letterSet){
        System.out.println(str);
    }

    System.out.println("\n");
    System.out.println("---Example of Hash Set---");
    //Demonstrates that a HashSet is an unordered collection
    Set hashSet = new HashSet();
    hashSet.add("Red");
    hashSet.add("Orange");
    hashSet.add("Yellow");
    hashSet.add("Green");

    for (Object str : hashSet){
        System.out.println(str);
    }
    System.out.println("\n");

        hashSet.add("Blue");
        hashSet.add("Purple");
        for (Object str : hashSet){
            System.out.println(str);
        }

        //This HashMap is ordered by keys
        System.out.println("\n---Example of Map---");
    Map map = new HashMap();
    map.put(1, "Five");
    map.put(2, "Four");
    map.put(3, "Three");
    map.put(4, "Two");
    map.put(3, "One");

    for (int i = 1; i < 5; i++){
        String number = (String)map.get(i);
        System.out.println(number);
    }

    //Using Generics, the program creates a list of Animal Objects
    System.out.println("\n");
    System.out.println("---LinkedList using Generics---");
    List<Animal> animalList = new LinkedList<>();
    animalList.add(new Animal("Arctic Wolf","carnivore", 175));
    animalList.add(new Animal("Black Bear","omnivore", 250));
    animalList.add(new Animal("Canadian Moose","herbivore", 1200));
    animalList.add(new Animal("Mule Deer","herbivore", 203));
    animalList.add(new Animal("Mountain Lion","carnivore", 150));
    animalList.add(new Animal("Raccoon","omnivore", 19));


    for (Animal a : animalList){
        System.out.println(a);
    }

    //This queue of Animal Objects sorts the animal by ascending weight
    System.out.println("\n");
    System.out.println("---Queue of Animals implementing AnimalComparator class organized by ascending weight---");
    Queue<Animal> queue = new PriorityQueue<>(6, new AnimalComparator());
    queue.add(new Animal("Arctic Wolf","carnivore", 175));
    queue.add(new Animal("Black Bear","omnivore", 250));
    queue.add(new Animal("Canadian Moose","herbivore", 1200));
    queue.add(new Animal("Mule Deer","herbivore", 203));
    queue.add(new Animal("Mountain Lion","carnivore", 150));
    queue.add(new Animal("Raccoon","omnivore", 19));

    Iterator iterator = queue.iterator();
    while(iterator.hasNext()){
        System.out.println(queue.poll());
    }








    }
}
